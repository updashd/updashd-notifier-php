<?php
namespace Updashd;

use Doctrine\ORM\Query\Expr\Join as ExprJoin;
use Predis\Client;
use Updashd\Configlib\Config;
use Updashd\Doctrine\Manager;
use Updashd\Model\Account;
use Updashd\Model\AccountNotifier;
use Updashd\Model\Incident;
use Updashd\Model\IncidentNotifierHistory;
use Updashd\Model\NodeService;
use Updashd\Model\NodeServiceNotifier;
use Updashd\Notifier\NoticeData;
use Updashd\Notifier\NotifierInterface;
use Updashd\Scheduler\Scheduler;

class Notifier {
    protected $notifiers = [];
    protected $config;
    protected $client;
    protected $manager;
    private $privateKey;

    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);

        // Create the Predis Client
        $client = new Client($this->getConfig('predis'));
        $this->setClient($client);

        $manager = new Manager($config['doctrine']);
        $this->setManager($manager);

        // Set up the workers
        foreach ($this->getConfig('implimentations') as $implementationFQCN) {
            $notifierName = forward_static_call(array($implementationFQCN, 'getNotifierName'));
            $this->setNotifier($notifierName, $implementationFQCN);
        }

        // Set up encryption
        $keyFile = $this->getConfig('private_key_file');
        $key = file_get_contents($keyFile);

        if (! $key) {
            throw new \Exception('Private key is required! Please see config/config.default.php');
        }

        $this->setPrivateKey($key);
    }

    /**
     * Run the worker
     */
    public function run () {
        $scheduler = new Scheduler($this->getClient(), $this->getConfig('zone'));

        while ($incidentId = $scheduler->getIncident()) {
            $this->handleIncident($incidentId);
        }
    }

    /**
     * @param int $incidentId
     */
    protected function handleIncident ($incidentId) {
        $incident = $this->getIncidentDetails($incidentId);

        $account = $incident->getAccount();

        $accountNotifiers = $this->getAccountNotifiers($account, $incident->getNodeService());

        foreach ($accountNotifiers as $accountNotifier) {
            $this->handleAccountNotifier($incident, $accountNotifier);
        }

        // Keep the memory cleaned up
        $em = $this->getManager()->getEntityManager();
        $em->clear();
    }

    /**
     * @param Incident $incident
     * @param AccountNotifier $accountNotifier
     */
    protected function handleAccountNotifier ($incident, $accountNotifier) {
        $em = $this->getManager()->getEntityManager();

        $incidentNotifierHistory = $this->getIncidentAccountHistory($incident, $accountNotifier);

        // Only send if we haven't done so in the last little while
        $send = true;

        if ($incidentNotifierHistory) {
            $lastTimestamp = $incidentNotifierHistory->getLastNotice()->getTimestamp();
            $minTimestamp = $lastTimestamp + $accountNotifier->getFrequency();

            // If the minimum timestamp is in the future
            if ($minTimestamp > time()) {
                // Don't send a message
                $send = false;
            }
        }

        if ($send) {
            // Get the Notifier's fully qualified class name
            $notifierFQCN = $this->getNotifier($accountNotifier->getNotifier()->getModuleName());

            // Create the account notifier configuration
            /** @var Config $notifierConfig */
            $notifierConfig = forward_static_call(array($notifierFQCN, 'createConfig'));

            $notifierConfig->setPrivateDecryptKey($this->getPrivateKey());

            // Load the account notifier config
            $notifierConfig->fromJson($accountNotifier->getConfig());

            // Create the Notifier Class
            /** @var NotifierInterface $notifier */
            $notifier = new $notifierFQCN($notifierConfig);

            $noticeData = new NoticeData();
            $noticeData->setIncident($incident);

            // SEND THE NOTICE
            $notifier->send($noticeData);

            // Record that a notice was sent
            if (! $incidentNotifierHistory) {
                $incidentNotifierHistory = new IncidentNotifierHistory();
                $incidentNotifierHistory->setIncident($incident);
                $incidentNotifierHistory->setAccountNotifier($accountNotifier);
            }

            $now = new \DateTime('now', new \DateTimeZone('UTC'));
            $incidentNotifierHistory->setLastNotice($now);

            // Save to the DB
            $em->persist($incidentNotifierHistory);
            $em->flush();

            // Clear up the memory
            $em->clear();
        }
    }

    /**
     * @param int $incidentId The id of the incident to retrieve.
     * @return Incident|null Incident if one is found, null if not found.
     */
    protected function getIncidentDetails ($incidentId) {
        $em = $this->getManager()->getEntityManager();

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('i, ns, z, n, s, e')
            ->from(Incident::class, 'i')
            ->innerJoin('i.nodeService', 'ns')
            ->innerJoin('i.zone', 'z')
            ->innerJoin('ns.node', 'n')
            ->innerJoin('ns.service', 's')
            ->innerJoin('n.environment', 'e');
//        $iqb = $qb->select('i, ns, z, n, s, e, rs, rm')
//            ->from(Incident::class, 'i')
//            ->innerJoin('i.nodeService', 'ns')
//            ->innerJoin('i.zone', 'z')
//            ->innerJoin('ns.node', 'n')
//            ->innerJoin('ns.service', 's')
//            ->innerJoin('n.environment', 'e')
//            ->leftJoin('i.results', 'rs')
//            ->leftJoin('rs.resultMetrics', 'rm');

        $iqb->where($iqb->expr()->eq('i.incidentId', ':incidentId'));

        $query = $iqb->getQuery();
        $query->setParameter('incidentId', $incidentId);

        /** @var Incident $incident */
        $incident = $query->getOneOrNullResult();

        return $incident;
    }

    /**
     * @param Incident $incident
     * @param AccountNotifier $accountNotifier
     * @return IncidentNotifierHistory
     */
    protected function getIncidentAccountHistory ($incident, $accountNotifier) {
        $em = $this->getManager()->getEntityManager();

        $inhRepo = $em->getRepository(IncidentNotifierHistory::class);

        /** @var IncidentNotifierHistory $inh */
        $inh = $inhRepo->findOneBy([
            'incident' => $incident,
            'accountNotifier' => $accountNotifier
        ]);

        return $inh;
    }

    /**
     * @param Account $account The account
     * @param NodeService $nodeService The node service
     * @return AccountNotifier[]|null Incident if one is found, null if not found.
     */
    protected function getAccountNotifiers ($account, $nodeService) {
        $em = $this->getManager()->getEntityManager();

        $qb = $em->createQueryBuilder();
        $iqb = $qb->select('an')
            ->from(AccountNotifier::class, 'an')
            ->innerJoin('an.account', 'a')
            ->leftJoin(NodeServiceNotifier::class, 'nsn', ExprJoin::WITH, $qb->expr()->eq('an', 'nsn.accountNotifier'))
            ->leftJoin('nsn.nodeService', 'ns');

        $iqb->where(
            $iqb->expr()->andX(
                // It is enabled AND
                $iqb->expr()->eq('an.isEnabled', 1), // AND
                // It is attached by
                $iqb->expr()->orX(
                    // The node service notifier OR
                    $iqb->expr()->eq('ns', ':nodeService'),

                    // The account notice is set as isDefault (meaning it runs for everything)
                    $iqb->expr()->andX(
                        $iqb->expr()->eq('a', ':account'),
                        $iqb->expr()->eq('an.isDefault', 1)
                    )
                )
            )
        );

        $query = $iqb->getQuery();
        $query->setParameter('nodeService', $nodeService);
        $query->setParameter('account', $account);

        $accountNotifiers = $query->getResult();

        return $accountNotifiers;
    }
    
    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    protected function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        
        return $this->config;
    }

    /**
     * Set the workerClassName for a given serviceName
     * @param $notifierName
     * @param $notifierClassName
     */
    protected function setNotifier ($notifierName, $notifierClassName) {
        $this->notifiers[$notifierName] = $notifierClassName;
    }

    /**
     * Get the service workerClassName for a given serviceName
     * @param $notifierName
     * @return mixed
     * @throws \Exception
     */
    protected function getNotifier ($notifierName) {
        if (! array_key_exists($notifierName, $this->notifiers)) {
            throw new \Exception('A notifier was requested for a service that is not provided.');
        }

        return $this->notifiers[$notifierName];
    }
    
    /**
     * @param array $config
     */
    protected function setConfig ($config) {
        $this->config = $config;
    }
    
    /**
     * @return Client
     */
    protected function getClient () {
        return $this->client;
    }
    
    /**
     * @param Client $client
     */
    protected function setClient (Client $client) {
        $this->client = $client;
    }

    /**
     * @return Manager
     */
    protected function getManager () {
        return $this->manager;
    }

    /**
     * @param Manager $manager
     */
    protected function setManager (Manager $manager) {
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    protected function getPrivateKey () {
        return $this->privateKey;
    }

    /**
     * @param string $privateKey
     */
    protected function setPrivateKey ($privateKey) {
        $this->privateKey = $privateKey;
    }
}