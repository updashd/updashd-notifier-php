<?php
chdir(__DIR__);

require 'vendor/autoload.php';

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ?: 'local';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;

    $config = array_replace_recursive($config, $envConfig);
}

$notifier = new \Updashd\Notifier($config);
$notifier->run();