<?php
$redisAddr = getenv('REDIS_ADDR') ?: '127.0.0.1';
$redisPort = getenv('REDIS_PORT') ?: 6379;
$redisPassword = getenv('REDIS_PASS') ?: null;

$predisParams = ['read_write_timeout' => 0];

if ($redisPassword) {
    $predisParams['password'] = $redisPassword;
}

$predisUri = 'tcp://' . $redisAddr . ':' . $redisPort . ($predisParams ? '?' . http_build_query($predisParams) : '');
return [
    'predis' => $predisUri,
    'zone' => getenv('UPDASHD_ZONE') ?: 'default',
    'private_key_file' => getenv('NOTIFIER_PRIV_KEY_FILE') ?: __DIR__ . '/key.pem',
    'implimentations' => [
        Updashd\Notifier\SlackNotifier::class
    ],
    'doctrine' => [
        'options' => [
            'dev_mode' => getenv('UPDASHD_DEBUG') ?: false,
            'simple_annotation' => false,
            'entity_paths' => [
                __DIR__ . '/../vendor/updashd/updashd-model/src'
            ],
            'proxy_dir' => __DIR__ . '/../data/doctrine_proxy'
        ],
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => getenv('MYSQL_HOST') ?: 'localhost',
            'dbname' => getenv('MYSQL_DB') ?: 'updashd',
            'user' => getenv('MYSQL_USER') ?: 'updashd',
            'port' => getenv('MYSQL_PORT') ?: 3306,
            'password' => getenv('MYSQL_PASS') ?: null,
        ]
    ]
];